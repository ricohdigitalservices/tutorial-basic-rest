var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var bodyParser = require('body-parser');
var http = require('http');
var exphbs = require('express-handlebars');


var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.set('views', path.join(__dirname, 'views'));
app.engine('hbs', exphbs({
    extname: 'hbs',
    defaultLayout: 'main.hbs'
}));
app.set('view engine', 'hbs');

var people = [
    {
        id: 1,
        name: "Nathan Thomas",
        jobTitle: "Developer",
        age: 29,
        relations: [
            {
                id: 3,
                name: "Jackie",
                jobTitle: "Teacher",
                age: 47
            }
        ]
    },
    {
        id: 2,
        name: "Charlie Lee",
        jobTitle: "Architect",
        age: 90,
        relations: []
    },
    {
        id: 3,
        name: "Jackie",
        jobTitle: "Teacher",
        age: 47,
        relations: []
    }
]

function findPerson(id) {
    var person;
    people.forEach(function (p) {
        if (p.id == id && !person) {
            person = p;
        }
    });
    return person;
}

app.get('/api/people', function (req, res) {
    res.json(people);
});

app.get('/api/people/:id/relations', function (req, res) {
    res.json(findPerson(req.params.id).relations);
});

app.get('/api/people/:id', function (req, res) {
    var person = findPerson(req.params.id);
    if (!person) {
        res.status(404).send('Person not found');
        return;
    }
    res.json(person);
});

app.post('/api/people', function (req, res) {
    people.push(req.body);
    res.json(people);
})

app.put('/api/people', function (req, res) {
    var person = findPerson(req.params.id);
    person.name = req.body.name;
    person.jobTitle = req.body.jobTitle;
    person.age = req.body.age;
    res.json(person);
});



var ipaddress = process.env.OPENSHIFT_NODEJS_IP;
var port = process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 8080;
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

// Start the server
var server = app.listen(port, server_ip_address, function () {
    var host = server.address().address;
    var port = server.address().port;
    console.log('app listening at http://%s:%s', host, port);
});

module.exports = app;